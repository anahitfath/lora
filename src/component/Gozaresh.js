import React, { useState, useEffect } from "react";
import { getallbuildings } from "../store/actions/mapAction";
import { getbuildingdevices } from "../store/actions/getBuildingDeviceAction";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { getSearchBuilding } from "../store/actions/getSearchAction";
import { getWord } from "../store/actions/getWordAction";
import { getSelect } from "../store/actions/getSelectedItemAction";
import user  from '../assets/img/user.svg'
import sharp  from '../assets/img/sharp.svg'
import meter  from '../assets/img/speedometer.svg'
import tag  from '../assets/img/tag.svg'
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const Gozaresh = () => {
  const [text, setText] = useState('');
  // const [devices, setDevices] = useState([]);
  // const [selectedDeviceState, setSelectedDeviceState] = useState(null);
  const [error, setError] = useState('');

  const dispatch = useDispatch();

  

  useEffect(() => {
    console.log(uuid)
    dispatch(getallbuildings());
    dispatch(getbuildingdevices(uuid));
  }, []); 

const uuid = useSelector((state) => state.getAllBuildings.data);
  console.log(uuid);

  const searchDevice = useSelector((state) => state.getSearchDevice.data);
  console.log(searchDevice);

  const word = useSelector((state) => state.getWord.data);
  console.log(word);


  const buildingDevices = useSelector((state) => state.getbuildingdevices.data);
  console.log(buildingDevices);

  const selectItem = useSelector((state) => state.getSelect.data);
  console.log(selectItem);


   function onSelectItem(device) {
    dispatch(getSelect(device));
    history.push(`/gozaresh/Taghvim/:${device.uuid}`); 
  };
 

   function onchangeinput (event)  {
  setText(event.target.value)
  };

  let history = useHistory();
    function onFormSubmit (event) {
    event.preventDefault();
    dispatch(getSearchBuilding(text));
    dispatch(getWord(text));
    if(text){
      history.push(`/gozaresh/SearchResult/:${text}`); 
    }
    else{    
      history.push(`/gozaresh`); 
      setError('شناسه کنتور خالی است')
    }
  };

  return ( <div className="gozareshbox">
                   <div className="errormessage d-flex justify-content-center">{error}</div>

        <form className="formbox d-flex justify-content-around align-items-center">
      
           <button className="searchbtn" onClick={onFormSubmit}>
           جستجو 
          </button>
           <input
             placeholder="شناسه کنتور مورد نظر را وارد کنید"
             type="number"
            value={text}
            onChange={onchangeinput}
             required
           />
           <span>جستجو بر اساس شناسه کنتور</span>
         </form>








         <div className="showbox">
          <div className="container d-flex flex-wrap devicecontainer mt-3">
        <div className="row justify-content-end">
        {buildingDevices.map((device) => {
              return (
                <div key={device.uuid} className="border col-lg-3 d-flex devicebox">
           
            <span>شناسه : {device.zipCode}
                  <img src={sharp} className="icon" alt="sharp"/>
                  </span>
                  <span>مالک : {device.owner}
                  <img src={user} className="icon" alt="user"/>
                  </span>
                  <span> {device.type} : نوع کنتور  
                  <img src={meter} className="icon" alt="meter"/>
                  </span>
                  <span>  {device.brand}:برند  
                  <img src={tag} className="icon" alt="tag"/>
                  </span> 
                    <span> نوع مصرف :  {device.subtype}  
                  <img src={tag} className="icon" alt="tag2"/>
                  </span>
         
                 {/* <span>{device.uuid}</span> */}
            

              <Router>
                    <button
                      onClick={() => onSelectItem(device)}
                      className="detailbtn mt-4"
                      type="button"
                    >مشاهده جزئیات
                    </button>
                  </Router> 



                </div>
              );
            })}
        </div>
          </div>
        </div>

  </div> );
}
 
export default Gozaresh;

// class Gozaresh extends Component {
//   state = { text: '', devices: [], selectedDeviceState: null ,error:''};

//   componentDidMount() {
//     this.props.getbuildingdevices({
//       id: this.props.uuid,
//     });
//     this.setState({ devices: this.props.buildingDevices });
//     this.props.getallbuildings();
//     console.log(this.props.uuid);
//   }

//   componentDidUpdate() {
//     this.props.getSelectedD(this.state.selectedDeviceState);
//     // console.log(this.props.selectedDevice);
//   }

//   onSelectItem = (device) => {
//     this.props.getSelect(device); 
//       //  window.location.reload(false);

//     const { history } = this.props;
//     history.push(`/gozaresh/Taghvim/:${device.uuid}`); 
//   };

//   onchangeinput = (event) => {
//     this.setState({ text: event.target.value });
  
//   };

  
//   onFormSubmit = (event) => {
//     event.preventDefault();
//     this.props.getSearchBuilding(this.state.text)
//     this.props.getWord(this.state.text)
//     console.log(this.props.searchDevice)
//     // const { history } = this.props;
//     // history.push(`/SearchResult/${this.state.text}`); 

//     if(this.state.text){
//       const { history } = this.props;
//       history.push(`/gozaresh/SearchResult/:${this.state.text}`); 
//     }
//     else{
//       const { history } = this.props;
//       history.push(`/gozaresh`); 
//       this.setState({error:'شناسه کنتور خالی است'})
//     }
//   };

//   render() {
//     console.log(this.props.selectItem)
//     return (
//       <div className="gozareshbox">
//               <div className="errormessage d-flex justify-content-center">{this.state.error}</div>

//         <form className="formbox d-flex justify-content-around align-items-center">
      
//            <button className="searchbtn" onClick={this.onFormSubmit}>
//            جستجو 
//            {/* <Link to={`/gozaresh/SearchResult/${this.state.text}`}>
//            جستجو             
//            </Link> */}
//           </button>
//           <input
//             placeholder="شناسه کنتور مورد نظر را وارد کنید"
//             type="number"
//             value={this.state.text}
//             onChange={this.onchangeinput}
//             required
//           />
//           <span>جستجو بر اساس شناسه کنتور</span>
//         </form>
//         <div className="showbox">
//           <div className="container d-flex flex-wrap devicecontainer mt-3">
//         <div className="row">
//         {this.state.devices.map((device) => {
//               return (
//                 <div key={device.uuid} className="border col-lg-3 d-flex devicebox">
           
//             <span>شناسه : {device.zipCode}
//                   <img src={sharp} className="icon" alt="sharp"/>
//                   </span>
//                   <span>مالک : {device.owner}
//                   <img src={user} className="icon" alt="user"/>
//                   </span>
//                   <span> {device.type} : نوع کنتور  
//                   <img src={meter} className="icon" alt="meter"/>
//                   </span>
//                   <span>  {device.brand}:برند  
//                   <img src={tag} className="icon" alt="tag"/>
//                   </span> 
//                     <span> نوع مصرف :  {device.subtype}  
//                   <img src={tag} className="icon" alt="tag2"/>
//                   </span>
         
//                  {/* <span>{device.uuid}</span> */}
            
//              <Router>
//                     <button
//                       onClick={() => this.onSelectItem(device)}
//                       className="detailbtn mt-4"
//                       type="button"
//                     >مشاهده جزئیات
//                       {/* <Link to={`/gozaresh/Taghvim/:${device.uuid}`}>
                        
//                       </Link> */}
//                     </button>
//                   </Router>

//                 </div>
//               );
//             })}
//         </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// const mapStateToProps = ({
//   getbuildingdevices,
//   getAllBuildings,
//   getSelectedD,
//   getSearchDevice,
//   getWord,
//   getSelect
// }) => {
//   return {
//     uuid: getAllBuildings.data,
//     buildingDevices: getbuildingdevices.data,
//     selectedDevice: getSelectedD.data,
//     searchDevice: getSearchDevice.data,
//     word:getWord.data,
//     selectItem:getSelect.data
//   };
// };


// export default compose(withRouter,connect(mapStateToProps,{
//   getallbuildings,
//   getbuildingdevices,
//   getSelectedD,
//   getSearchBuilding,
//   getWord,
//   getSelect
// }))(Gozaresh)


