import React, { useState, useEffect } from "react";
// import { getSelect } from "../store/actions/getSelectedItemAction";
// import { getbill } from "../store/actions/getbillAction";
// import { getTime } from "../store/actions/getbillTimeAction";
import ShowCalendar from "./ShowCalendar";
// import { BrowserRouter as Router, Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import waterimage from '../assets/img/waterimg.jpg'
import gasimage from '../assets/img/gasimg.jpg'
import elecimage from '../assets/img/electricityimg.jpg'

const Calendar = () => {

  const [owner, setOwner] = useState('');
  const [type3, setType3] = useState('');
  const [zipCode, setZipcode] = useState('');
  const [brand, setBrand] = useState('');
  const [imagesrc, setImagesrc] = useState('');
  const [uuid, setUuid] = useState('');
  const [errormessage, setErrormessage] = useState('');
  const [begintimeunix, setBegintimeunix] = useState('');
  const [endtimeunix, setEndtimeunix] = useState('');


  let history = useHistory();


  const dispatch = useDispatch();

  
  const time = useSelector((state) => state.getTime.data);
  console.log(time);

  useEffect(() => {
    console.log(selectItem)
    setOwner(selectItem.owner);
    setType3(selectItem.type);
    setZipcode(selectItem.zipCode);
    setBrand(selectItem.brand);
    setUuid(selectItem.uuid);
      console.log(zipCode)


    if (selectItem.type === "water") {
      setImagesrc(waterimage)
    } else if (selectItem.type === "gas") {
      setImagesrc(gasimage)
    } else {
      setImagesrc(elecimage)
    }

console.log(time.endTime)

  }, [time]); 

  const selectItem = useSelector((state) => state.getSelect.data);
  console.log(selectItem);

  const bill = useSelector((state) => state.getbill.data);
  console.log(bill);



let billbtnclass;
if(time.endTime){
  billbtnclass='completePeriod'
}
else{
  billbtnclass='showbill'
}


  
 function showbill(){
    if(time.endTime!==null){
      history.push(`/gozaresh/Taghvim/bill/:${uuid}`); 
    }
    else{
      setErrormessage('بازه زمانی را انتخاب کنید')
    }
  }


console.log(uuid)

  return (
           <div className="calendarbox ">
          <div className="itembox container pt-3 pb-3">
            <div className="row">
              <div className="col-lg-6 left">
                <span className="description">
                  ابتدا و انتهای بازه زمانی را مشخص کنید
                </span>
  
                <ShowCalendar uuid={uuid}/>
              </div>
              <div className="col-lg-6 d-flex flex-column taghvimdetail right">
                <div className="d-flex flex-column detailcontor">
                  <span>

                    <img
                      src={imagesrc}
                      className="logoimage"
                      alt="logoimage"
                    />

  {/* <img
                      src={waterimage}
                      className="logoimage"
                      alt="logoimage"
                    /> */}
                    
                   
               
                  </span>
                  <div className="d-flex flex-column ">
                    <span>مالک : {owner}</span>
                    <span> {type3} : نوع کنتور </span>
                    <span>شناسه : {zipCode}</span>
                    <span> {brand} : برند </span>
                    <span> {uuid} : کد </span>

                  </div>
                  <div className="erroemessage">{errormessage}</div>
                  <span>
                    <button className={billbtnclass} onClick={showbill}>
                        نمایش قبض
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div> );
}
 
export default Calendar;


// class Calendar extends Component {
//   state = {
//     owner: "",
//     type3: "",
//     zipcode: "",
//     brand: "",
//     imagesrc: "",
//     uuid:'',
//     errormessage:'',
//     begintimeunix:'',
//     endtimeunix:''
//   };
//   componentDidMount() {
//     console.log(this.props.selectItem);
//     this.setState({ owner: this.props.selectItem.owner });
//     this.setState({ type3: this.props.selectItem.type });
//     this.setState({ zipcode: this.props.selectItem.zipCode });
//     this.setState({ brand: this.props.selectItem.brand });
//     this.setState({ uuid: this.props.selectItem.uuid });



//     if (this.props.selectItem.type === "water") {
//       this.setState({ imagesrc: require("../assets/img/waterimg.jpg") });
//     } else if (this.props.selectItem.type === "gas") {
//       this.setState({ imagesrc: require("../assets/img/gasimg.jpg") });
//     } else {
//       this.setState({ imagesrc: require("../assets/img/electricityimg.jpg") });
//     }
//   }

//   showbill=()=>{
//     //       const { history } = this.props;
//     // history.push(`/gozaresh/Taghvim/bill/:${this.state.uuid}`); 
//     // if(this.props.)
//     if(this.props.time.endTime!==null){
//       const { history } = this.props;
//       history.push(`/gozaresh/Taghvim/bill/:${this.state.uuid}`); 
//     }
//     else{
//       this.setState({errormessage:'بازه زمانی را انتخاب کنید'})
//     }
//   }


  

//   render() {
//     console.log(this.props.time.endTime)
//     return (
//       <div className="calendarbox ">
//         <div className="itembox container pt-3 pb-3">
//           <div className="row">
//             <div className="col-lg-6 left">
//               <span className="description">
//                 ابتدا و انتهای بازه زمانی را مشخص کنید
//               </span>

//               <ShowCalendar uuid={this.state.uuid}/>
//             </div>
//             <div className="col-lg-6 d-flex flex-column taghvimdetail right">
//               <div className="d-flex flex-column detailcontor">
//                 <span>
//                   <img
//                     src={this.state.imagesrc}
//                     className="logoimage"
//                     alt="logoimage"
//                   />
//                 </span>
//                 <div className="d-flex flex-column ">
//                   <span>مالک : {this.state.owner}</span>
//                   <span> {this.state.type3} : نوع کنتور </span>
//                   <span>شناسه : {this.state.zipcode}</span>
//                   <span> {this.state.brand} : برند </span>
//                   {/* <span> {this.state.uuid} : برند </span> */}
//                 </div>
//                 <div className="erroemessage">{this.state.errormessage}</div>
//                 <span>
//                   <button className="showbill mt-4" onClick={this.showbill}>
//                   {/* <Link to={`/gozaresh/Taghvim/bill/:${this.state.uuid}`}>
//                   نمایش قبض
//                       </Link> */}
//                       نمایش قبض
//                   </button>
//                 </span>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// const mapStateToProps = ({ getSelect,getbill,getTime }) => {
//   return {
//     selectItem: getSelect.data,
//     bill:getbill.data,
//     time:getTime.data
//   };
// };

// export default compose(
//   withRouter,
//   connect(mapStateToProps, {
//     getSelect,
//     getbill,
//     getTime
//   })
// )(Calendar);
