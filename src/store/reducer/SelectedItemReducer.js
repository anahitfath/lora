import {GET_SELECTED_ITEM} from '../../Type/Types'

const INITIAL_STATE ={
    data:''
}


export default function getSelect( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_SELECTED_ITEM:
            // console.log(action.payload.data.data[0].uuid)
            return {
                data:action.payload
    };
   
        default:
            return state;
    }
    
    
}
