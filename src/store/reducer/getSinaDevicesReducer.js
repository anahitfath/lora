import {GET_Asset_Devices} from '../../Type/Types'

const INITIAL_STATE ={
    data:[]
}

export default function getSinaDevicesReducer( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_Asset_Devices:
            // console.log(action.payload.data.data.length)
            return {
                
                data:action.payload.data.data.length
    };
   
        default:
            return state;
    }
    
    
}
