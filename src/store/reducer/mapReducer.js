import {GET_ALL_BUILDINGS} from '../../Type/Types'

const INITIAL_STATE ={
    data:''
}


export default function getAllBuildings( state= INITIAL_STATE, action){
    switch(action.type){
        case GET_ALL_BUILDINGS:
            // console.log(action.payload.data.data[0].uuid)
            return {
                data:action.payload.data.data[0].uuid
    };
   
        default:
            return state;
    }
    
    
}
