import { GET_ALL_BUILDINGS } from "../../Type/Types";
import axios from "axios";

const getallbuildingsapi = (data) => ({
  type: GET_ALL_BUILDINGS,
  payload: data,
});

export const getallbuildings = () => {
  return (dispatch) => {
    axios
      .get("https://api.lora.atrovan.com/api/buildings/all?password=asd123")

      .then((data) => {
        // console.log(data);
        dispatch(getallbuildingsapi(data));
      })
      .catch((e) => {});
  };
};
