import { GET_SELECTED_ASSET } from "../../Type/Types";


const getSelectedAsset = (data) => ({
  type: GET_SELECTED_ASSET,
  payload: data,
});

export const getSelected = (select) => {
  return (dispatch) => {
//    console.log(select)
        dispatch(getSelectedAsset(select));
    
  };
};