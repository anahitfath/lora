import { GET_BUILDINGS_Devices } from "../../Type/Types";
import axios from "axios";

const getbuildingdevicesapi = (data) => ({
  type: GET_BUILDINGS_Devices,
  payload: data,
});

export const getbuildingdevices = (uuid) => {
  return (dispatch) => {
    axios
      .get(`https://api.lora.atrovan.com/api/devices/building/${uuid}?limit=100`)
      
      .then((data) => {
        // console.log(data);
        dispatch(getbuildingdevicesapi(data));
      })
      .catch((e) => {});
  };
};

